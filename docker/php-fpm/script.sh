#!/bin/sh

php artisan migrate:fresh --seed
chown -R www-data:www-data storage/
chmod -R 775 storage
php artisan queue:work